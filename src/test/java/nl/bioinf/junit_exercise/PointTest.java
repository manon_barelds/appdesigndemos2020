package nl.bioinf.junit_exercise;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {

    @Test
    void distanceToSunny() {
        Point one = new Point(0,0);
        Point two = new Point(4, 3);
        double expectedDistance = 5;
        double actualDistance = one.distanceTo(two);

        assertEquals(expectedDistance, actualDistance);
    }

    @Test
    void distanceToWithNullInput() {
        Point one = new Point(0,0);
        Point two = null;

        final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                () -> one.distanceTo(two));
        assertEquals("null input provided for Point", ex.getMessage());
    }

    @Test
    @Disabled
        //TODO not relevant?
    void distanceToPrimitiveBoundary() {
        Point one = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Point two = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);

        try{
            final double distance = one.distanceTo(two);
            fail();
        } catch (ArithmeticException ex) {
            assertEquals("Integer overflow", ex.getMessage());
        }
    }


    //@Test
    @ParameterizedTest
    @CsvSource({"0, 0, 4, 3, 5", "10, 20, 15, 30, 11.18034"})
    void distanceToSunnyParams(int oneX, int oneY, int twoX, int twoY, double expected) {
        System.out.println("oneX = " + oneX);
        System.out.println("oneY = " + oneY);
        System.out.println("twoX = " + twoX);
        System.out.println("twoY = " + twoY);
        System.out.println("expected = " + expected);
        Point one = new Point(oneX,oneY);
        Point two = new Point(twoX, twoY);
        double actualDistance = one.distanceTo(two);

        assertEquals(expected, actualDistance, 1E-7);
    }
}