package nl.bioinf.junidemo;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilTest {

    @BeforeEach
    void beforeEachCode() {
        System.out.println("Running before each test");
    }

    @BeforeAll
    static void beforeAlltest() {
        System.out.println("before all of this class");
    }

    @Test
    @DisplayName("Sunny Day scenario")
    void calculateSurfaceSunnyDay() {
        double observedSurface = Util.calculateSurface(2, 3);
        double expectedSurface = 6;
        assertEquals(expectedSurface, observedSurface);

        observedSurface = Util.calculateSurface(4, 5);
        expectedSurface = 4*5;
        assertEquals(expectedSurface, observedSurface);
    }

    @Test
    void calculateSurfaceZeroInputs() {
        final double observedSurface = Util.calculateSurface(0, 0);
        final double expectedSurface = 0;
        assertEquals(expectedSurface, observedSurface);
    }

//    @Test
//    void calculateSurfaceNegativeInputs() {
//
//    }

}