package nl.bioinf.junidemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Teacher {
    private List<String> students = new ArrayList<>();

    public List<String> getStudents() {
        return Collections.unmodifiableList(students);
    }

    public void setStudents(List<String> students) {
        this.students.addAll(students);
    }
}
