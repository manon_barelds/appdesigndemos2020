package nl.bioinf.drugtrial;

public class Subject{
    private String id;

    public Subject(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getResponse(Drug drug) {
        //I want to know which drug!
        System.out.println(drug.getClass().getName());
        System.out.println(drug.getClass().getSimpleName());
        System.out.println(drug.getClass().getCanonicalName());
        System.out.println(drug.getClass().getTypeName());

//        if (drug instanceof Placebo) {
            Placebo placebo = (Placebo)drug;
            placebo.placeboResponse();
//            return 100;
//        }

        //delegation!
        return drug.getResponse();
    }
}
