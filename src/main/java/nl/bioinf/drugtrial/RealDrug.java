package nl.bioinf.drugtrial;

public class RealDrug implements Drug {

    @Override
    public int getResponse() {
        return 100;
    }
}
