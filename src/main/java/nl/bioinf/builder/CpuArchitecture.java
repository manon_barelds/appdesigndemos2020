package nl.bioinf.builder;

public enum CpuArchitecture {
    QUAD_CORE_I7,
    QUAD_CORE_I5,
    OCTACORE_I7,
    OCTA_CORE_I5,
    DUAL_CORE_I5;
}
