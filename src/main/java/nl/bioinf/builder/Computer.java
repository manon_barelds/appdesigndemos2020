package nl.bioinf.builder;

public class Computer {
    //required
    private int ramInGigaBytes;
    //required
    private int ssdInGigaBytes;
    //required
    private CpuArchitecture cpuArchitecture;

    //optional
    private boolean withKeyBoard;
    //optional
    private String monitorMakeAndModel;

}
