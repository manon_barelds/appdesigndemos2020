package nl.bioinf.factory;

public class Digit {
    private static final Digit[] CACHE = new Digit[10];
    private final int value;

    static {
        for (int i = 0; i < 10; i++) {
            Digit d = new Digit(i);
            CACHE[i] = d;
        }
    }

    public int getValue() {
        return value;
    }

    private Digit(int value) {
        this.value = value;
    }

    public static Digit of(String digitStr) {
        try {
            int value = Integer.parseInt(digitStr);
            if (value < 0 || value > 9) {
                throw new IllegalArgumentException("number is not a single digit!");
            }
            return CACHE[value];
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("argument is not a number!");
        }
    }
}
