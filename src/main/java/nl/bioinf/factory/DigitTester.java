package nl.bioinf.factory;

public class DigitTester {
    public static void main(String[] args) {
        //note that the Object hash is the same in each case.
        //this is because the objects are cached
        Digit d1 = Digit.of("4");
        System.out.println("d1 = " + d1);

        Digit d2 = Digit.of("4");
        System.out.println("d2 = " + d2);
        System.out.println("d1 is the same as d2:" + (d1 == d2));

        Digit d3 = Digit.of("4");
        System.out.println("d3 = " + d3);
    }
}
