package nl.bioinf.birds;

public class NoFlightException extends RuntimeException {
    public NoFlightException(String message) {
        super(message);
    }
}
